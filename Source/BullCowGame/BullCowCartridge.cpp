// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"

void UBullCowCartridge::BeginPlay() // When the game starts
{
	Super::BeginPlay();

	// Loading text file to add words into array
	const FString WordListPath = FPaths::ProjectContentDir() / TEXT("WordsList.txt");
	FFileHelper::LoadFileToStringArray(HiddenIsograms, *WordListPath);

	SetupGame();
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
	ClearScreen();

	if (bGameOver)
	{
		SetupGame();
		return;
	}

	ProcessGuess(Input);
}

void UBullCowCartridge::SetupGame()
{
	// Welcoming player
	PrintLine(TEXT("Welcome to the Bull and Cows !"));

	HiddenWord = HiddenIsograms[FMath::RandRange(0, HiddenIsograms.Num() - 1)];
	Lives = HiddenWord.Len();
	bGameOver = false;

	PrintLine(TEXT("Guess the %d letter isogram !"), HiddenWord.Len());
	PrintLine(TEXT("The number of possible words are %i."), HiddenIsograms.Num());
	PrintLine(TEXT("You have %i lives."), Lives);

	PrintLine(TEXT("Type in your guess.\nPress Enter to continue..."));
}

void UBullCowCartridge::EndGame()
{
	bGameOver = true;
	PrintLine(TEXT("Press Enter to Play Again !"));
}

void UBullCowCartridge::ProcessGuess(const FString& Guess)
{
	ClearScreen();

	if (Guess == HiddenWord)
	{
		PrintLine(TEXT("YOU WON !"));
		EndGame();
		return;
	}

	if (Guess.Len() != HiddenWord.Len()) // Not punish
	{
		PrintLine(TEXT("YOU FOOL ! Try guessing with the correct number of characters !"), Lives);
		return;
	}

	if (!IsIsogram(Guess)) // Not punish
	{
		PrintLine("NO REPEATING LETTERS ! Try Again !");
		return;
	}

	--Lives;
	PrintLine(TEXT("Wrong ! You lost a life !"));
	if (Lives <= 0)
	{
		PrintLine(TEXT("You have no lives left !"));
		PrintLine(TEXT("The hidden word was: %s"), *HiddenWord);
		EndGame();
		return;
	}
	// Show the player Bulls and Cows
	FBullCowCount BullCowCounter;
	GetBullCows(Guess, BullCowCounter);

	PrintLine(TEXT("You have %i Bulls and %i Cows"), BullCowCounter.BullCount, BullCowCounter.CowCount);

	PrintLine(TEXT("You have %i lives left !"), Lives);
	PrintLine(TEXT("Guess again !"));
}

bool UBullCowCartridge::IsIsogram(const FString& Word) const
{
	for (int32 i = 0; i < Word.Len(); i++)
	{
		for (int32 j = i + 1; j < Word.Len(); j++)
		{
			if (Word[i] == Word[j])
				return false;
		}
	}
	return true;
}

void UBullCowCartridge::GetBullCows(const FString& Guess, FBullCowCount& BullCowCount) const
{
	// for every index Guess is same as index Hidden, BullCount++
	// if not a bull was it a cow ? if yes CowCount++

	for (int32 GuessIndex = 0; GuessIndex < Guess.Len(); GuessIndex++)
	{
		if (Guess[GuessIndex] == HiddenWord[GuessIndex])
		{
			BullCowCount.BullCount++;
			continue;
		}
		for (int32 HiddenIndex = 0; HiddenIndex < HiddenWord.Len(); HiddenIndex++)
		{
			if (Guess[GuessIndex] == HiddenWord[HiddenIndex])
			{
				BullCowCount.CowCount++;
				break;
			}
		}
	}
}
