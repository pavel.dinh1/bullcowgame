// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "BullCowCartridge.generated.h"

USTRUCT()
struct FBullCowCount
{
	GENERATED_BODY()

public:
	int32 BullCount;
	int32 CowCount;

	FBullCowCount()
	{
		BullCount = 0;
		CowCount = 0;
	}
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;

	void SetupGame();
	void EndGame();
	void ProcessGuess(const FString& Guess);
	bool IsIsogram(const FString& Word) const;
	void GetBullCows(const FString& Guess, FBullCowCount& BullCowCount) const;

	// Your declarations go below!
	private:
		TArray<FString> HiddenIsograms;
		FString HiddenWord;
		int32 Lives;
		bool bGameOver;
};
